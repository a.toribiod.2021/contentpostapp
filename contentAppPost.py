#!/usr/bin/env python3

# ContentApp class
# Simple web application for serving content
#
# Copyright Jesus M. Gonzalez-Barahona 2022
# jgb @ gsyc.es
# SARO, SAT, ST subjects (Universidad Rey Juan Carlos)

import webapp

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    {content}
    <form action="" method="get" class="form-example">
        <div class="form-example">
            <label for="name">Enter update: </label>
            <input type="text" name="name" id="name" required />
        </div>
        <div class="form-example">
            <input type="submit" value="Update" />
        </div>
    </form>
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Resource not found: {resource}.</p>
  </body>
</html>
"""


class ContentApp(webapp.webApp):

    contents = {'/': "<p>Main page</p>",
                '/hello': "<p>Hello, people</p>",
                '/bye': "<p>Bye all!!</p>"}

    def parse(self, request):
        """Return the resource name"""
        if len(request.split()) > 1:
            result = {"resource": request.split()[1], "content": request.split("\n")[-1], "method": request.split()[0]}
            print("RECURSO:", result["resource"])
            print("CONTENT:", result["content"])
            print("METHOD:", result["method"])
            return result

    def process(self, resource):
        """Produce the page with the content for the resource"""
        if resource["method"] == "GET":
            if resource["resource"] in self.contents:
                content = self.contents[resource["resource"]]
                page = PAGE.format(content=content)
                code = "200 OK"
            else:
                page = PAGE_NOT_FOUND.format(resource=resource)
                code = "404 Resource Not Found"
            return code, page
        elif resource["method"] == "POST":
            self.contents.update({resource["resource"]: resource["content"]})
            print(self.contents)
            content = self.contents[resource["resource"]]
            page = PAGE.format(content=content)
            code = "200 OK"
            return code, page


if __name__ == "__main__":
    webApp = ContentApp("localhost", 1234)
